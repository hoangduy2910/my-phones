import dotenv from "dotenv";

dotenv.config();

const MONGO_OPTIONS = {
  useUnifiedTopology: true,
  useNewUrlParser: true,
  socketTimeoutMS: 30000,
  keepAlive: true,
  autoIndex: false,
  retryWrites: false,
};

const MONGO_USERNAME = process.env.MONGO_USERNAME || "admin";
const MONGO_PASSWORD = process.env.MONGO_PASSWORD || "admin";
const MONGO_URL =
  process.env.MONGO_URL ||
  "cluster0.po6sj.mongodb.net/my-phones?retryWrites=true&w=majority";

export const MONGO_CONFIG = {
  HOST: MONGO_URL,
  USERNAME: MONGO_USERNAME,
  PASSWORD: MONGO_PASSWORD,
  OPTIONS: MONGO_OPTIONS,
  URL: `mongodb+srv://${MONGO_USERNAME}:${MONGO_PASSWORD}@${MONGO_URL}`,
};
