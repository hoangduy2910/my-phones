import express from "express";
import dotenv from "dotenv";
import mongoose from "mongoose";

import { MONGO_CONFIG } from "./configs/mongo";
import { LOGGING } from "./utils/logging";
import { trim } from "./middlewares/trim";
import { logger } from "./middlewares/logger";
import { errorHandler } from "./middlewares/error";

import { userRoutes } from "./routes/user";
import { notFoundRoute } from "./routes/notFound";

const NAMESPACE = "SERVER";
const app = express();

// Config Server
dotenv.config();
const HOST = process.env.HOST || "localhost";
const PORT = process.env.PORT ? parseInt(process.env.PORT) : 8000;

// Middlewares;
app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use(trim);
app.use(logger);

// Routes
app.use("/api/users", userRoutes);

// Not Found
app.get("*", notFoundRoute);

// Handle Error
app.use(errorHandler);

// Connect MongoDB
app.listen(PORT, () => {
  LOGGING.INFO(NAMESPACE, `Running on http://${HOST}:${PORT}`);
});

mongoose
  .connect(MONGO_CONFIG.URL, MONGO_CONFIG.OPTIONS)
  .then(() => LOGGING.INFO(NAMESPACE, "Connected to MongoDB"))
  .catch((err) => LOGGING.ERROR(NAMESPACE, err.message, err));
