const EMAIL_INVALID = "Email không hợp lệ";
const EMAIL_EXIST = "Email đã tồn tại";
const EMAIL_NOT_EXIST = "Email không tồn tại";

const PASSWORD_INVALID = "Mật khẩu không hợp lệ";
const PASSWORD_MIN_LENGTH = "Mật khẩu phải có hơn 6 ký tự";
const PASSWORD_NOT_MATCH = "Mật khẩu và mật khẩu xác thực không đúng";

const CONFIRM_PASWORD_EMPTY = "Mật khẩu xác thực không được để trống";
const CONFIRM_PASWORD_MIN_LENGTH = "Mật khẩu xác thực phải có hơn 6 ký tự";

const FIRSTNAME_EMPTY = "Tên không được để trống";
const LASTNAME_EMPTY = "Họ không được để trống";

const PHONE_NUMBER_EMPTY = "Số điện thoại không được để trống";
const PHONE_NUMBER_INVALID = "Số điện thoại không hợp lệ";

const ADDRESS_EMPTY = "Địa chỉ không được để trống";

const USER_NOT_FOUND = "Không tìm thấy người dùng";
const USER_NOT_ALLOW = "Hành động không hợp lệ";

export const userConstants = {
  EMAIL_INVALID,
  EMAIL_EXIST,
  EMAIL_NOT_EXIST,
  PASSWORD_MIN_LENGTH,
  PASSWORD_INVALID,
  PASSWORD_NOT_MATCH,
  CONFIRM_PASWORD_EMPTY,
  CONFIRM_PASWORD_MIN_LENGTH,
  FIRSTNAME_EMPTY,
  LASTNAME_EMPTY,
  PHONE_NUMBER_EMPTY,
  PHONE_NUMBER_INVALID,
  ADDRESS_EMPTY,
  USER_NOT_FOUND,
  USER_NOT_ALLOW,
};
