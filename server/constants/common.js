const INTERNAL_SERVER_ERROR =
  "Có lỗi xảy ra trong quá trình thực hiện. Vui lòng thử lại sau";
const UNAUTHENTICATED = "Người dùng chưa xác thực. Vui lòng đăng nhập.";
const NOT_FOUND = "Rất tiếc không tìm thấy đường dẫn được yêu cầu";
const METHOD_NOT_ALLOW = "";

export const commonConstants = {
  INTERNAL_SERVER_ERROR,
  UNAUTHENTICATED,
  NOT_FOUND,
};
