import chalk from "chalk";
import { getTimeStamp } from "../utils/utils";

const INFO = (namespace, message, object) => {
  const date = chalk.bold.magentaBright(`[${getTimeStamp()}]`);
  const messageType = chalk.bold.blueBright(`[INFO] [${namespace}] ${message}`);

  if (object) {
    console.log(`${date} ${messageType}`, object);
  } else {
    console.log(`${date} ${messageType}`);
  }
};

const WARN = (namespace, message, object) => {
  const date = chalk.bold.magentaBright(`[${getTimeStamp()}]`);
  const messageType = chalk.bold.yellowBright(
    `[WARN] [${namespace}] ${message}`
  );

  if (object) {
    console.log(`${date} ${messageType}`, object);
  } else {
    console.log(`${date} ${messageType}`);
  }
};

const ERROR = (namespace, message, object) => {
  const date = chalk.bold.magentaBright(`[${getTimeStamp()}]`);
  const messageType = chalk.bold.redBright(`[ERROR] [${namespace}] ${message}`);

  if (object) {
    console.log(`${date} ${messageType}`, object);
  } else {
    console.log(`${date} ${messageType}`);
  }
};

const DEBUG = (namespace, message, object) => {
  const date = chalk.bold.magentaBright(`[${getTimeStamp()}]`);
  const messageType = chalk.bold.greenBright(
    `[DEBUG] [${namespace}] ${message}`
  );

  if (object) {
    console.log(`${date} ${messageType}`, object);
  } else {
    console.log(`${date} ${messageType}`);
  }
};

export const LOGGING = {
  INFO,
  WARN,
  ERROR,
  DEBUG,
};
