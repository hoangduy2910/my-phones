import { StatusCodes } from "http-status-codes";
import { commonConstants } from "../constants/common";

export const notFoundRoute = (req, res, next) => {
  return res.status(StatusCodes.NOT_FOUND).json({
    status: StatusCodes.NOT_FOUND,
    message: commonConstants.NOT_FOUND,
    error: null,
  });
};
