import express from "express";
import { UserController } from "../controllers/user";
import { userValidator } from "../validators/user";
import { validation } from "../middlewares/validation";

export const userRoutes = express.Router();
userRoutes.post(
  "/register",
  userValidator.register(),
  validation,
  UserController.register
);
userRoutes.post(
  "/login",
  userValidator.login(),
  validation,
  UserController.login
);
userRoutes.get("/:id", UserController.getById);
userRoutes.patch(
  "/:id",
  userValidator.update(),
  validation,
  UserController.update
);
userRoutes.post(
  "/:id/reset-password",
  userValidator.resetPassword(),
  validation,
  UserController.resetPassword
);
