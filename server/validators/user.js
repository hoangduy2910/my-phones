import { check } from "express-validator";
import { userConstants } from "../constants/user";

const login = () => {
  return [
    check("email", userConstants.EMAIL_INVALID)
      .normalizeEmail({ gmail_remove_dots: false })
      .isEmail(),
    check("password", userConstants.PASSWORD_MIN_LENGTH).isLength({
      min: 6,
    }),
  ];
};

const register = () => {
  return [
    check("email", userConstants.EMAIL_INVALID)
      .normalizeEmail({ gmail_remove_dots: false })
      .isEmail(),
    check("password", userConstants.PASSWORD_MIN_LENGTH).isLength({
      min: 6,
    }),
    check("password", userConstants.PASSWORD_NOT_MATCH).custom(
      (value, { req }) => value === req.body.confirm
    ),
    check("confirm", userConstants.CONFIRM_PASWORD_MIN_LENGTH).isLength({
      min: 6,
    }),
    check("confirm", userConstants.PASSWORD_NOT_MATCH).custom(
      (value, { req }) => value === req.body.password
    ),
    check("phoneNumber", userConstants.PHONE_NUMBER_INVALID).isLength({
      min: 10,
      max: 10,
    }),
  ];
};

const update = () => {
  return [
    check("email", userConstants.EMAIL_INVALID)
      .normalizeEmail({ gmail_remove_dots: false })
      .isEmail(),
    check("firstName", userConstants.FIRSTNAME_EMPTY).notEmpty(),
    check("lastName", userConstants.LASTNAME_EMPTY).notEmpty(),
    check("address", userConstants.ADDRESS_EMPTY).notEmpty(),
    check("phoneNumber", userConstants.PHONE_NUMBER_INVALID).isLength({
      min: 10,
      max: 10,
    }),
  ];
};

const resetPassword = () => {
  return [
    check("password", userConstants.PASSWORD_MIN_LENGTH).isLength({
      min: 6,
    }),
    check("password", userConstants.PASSWORD_NOT_MATCH).custom(
      (value, { req }) => value === req.body.confirm
    ),
    check("confirm", userConstants.CONFIRM_PASWORD_MIN_LENGTH).isLength({
      min: 6,
    }),
    check("confirm", userConstants.PASSWORD_NOT_MATCH).custom(
      (value, { req }) => value === req.body.password
    ),
  ];
};

export const userValidator = {
  login,
  register,
  update,
  resetPassword,
};
