import mongoose from "mongoose";

const userSchema = new mongoose.Schema(
  {
    email: { type: String, required: true, unique: true },
    password: { type: String, required: true, minlength: 6 },
    firstName: { type: String, required: false },
    lastName: { type: String, required: false },
    image: { type: String, required: true },
    address: { type: String, required: false },
    phoneNumber: { type: String, required: true },
  },
  { timestamps: true, toObject: { getters: true } }
);

userSchema.options.toObject = {
  transform: function (doc, user, options) {
    user.id = user._id;
    delete user._id;
    delete user.__v;
    delete user.password;
    delete user.createdAt;
    delete user.updatedAt;
    return user;
  },
};

export const UserModel = mongoose.model("User", userSchema);
