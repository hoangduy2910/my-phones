import chalk from "chalk";
import { getTimeStamp } from "../utils/utils";

const getMessage = (method, url, code = "", elapsed = "") => {
  return chalk.bold.greenBright(
    `${chalk.bold.magentaBright(
      `[${getTimeStamp()}]`
    )} [${method}] ${url} ${code} ${elapsed}`
  );
};

export const logger = async (req, res, next) => {
  console.log(getMessage(req.method, req.originalUrl));

  const start = new Date().getTime();
  res.on("finish", () => {
    const elapsed = new Date().getTime() - start;
    console.log(
      getMessage(req.method, req.originalUrl, res.statusCode, `${elapsed}ms`)
    );
  });

  next();
};
