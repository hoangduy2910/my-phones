import { validationResult } from "express-validator";
import { ResponseError } from "../common/models/response";

export const validation = (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    const err = errors.array().reduce((errorObj, err) => {
      errorObj[err.param] = err.msg;
      return errorObj;
    }, {});

    return next(new ResponseError(err));
  }

  next();
};
