import { StatusCodes } from "http-status-codes";
import { commonConstants } from "../constants/common";

export const errorHandler = (error, req, res, next) => {
  let errorRes = {};
  
  if (typeof error === "object") {
    errorRes = error;
  } else if (typeof error === "string") {
    errorRes = {
      status: StatusCodes.OK,
      message: error,
      error: null,
    };
  } else {
    errorRes = {
      status: StatusCodes.INTERNAL_SERVER_ERROR,
      message: commonConstants.INTERNAL_SERVER_ERROR,
      error: null,
    };
  }

  return res.status(errorRes.status).json(errorRes);
};
