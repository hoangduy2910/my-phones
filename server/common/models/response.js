export class ResponseSuccess {
  constructor(data, statusCode = 200) {
    this.status = statusCode;
    this.message = "";
    this.data = data;
  }
}

export class ResponseError extends Error {
  constructor(error, statusCode = 200) {
    super();

    if (typeof error === "string") {
      this.status = statusCode;
      this.message = error;
      this.error = null;
    } else if (typeof error === "object") {
      this.status = statusCode;
      this.message = "";
      this.error = error;
    } else {
      this.status = statusCode;
      this.message = "";
      this.error = null;
    }
  }
}
