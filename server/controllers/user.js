import fs from "fs";
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";
import dotenv from "dotenv";
import { StatusCodes } from "http-status-codes";

import { ResponseSuccess, ResponseError } from "../common/models/response";
import { UserModel } from "../models/user";
import { userConstants } from "../constants/user";

dotenv.config();

const register = async (req, res, next) => {
  try {
    const user = await UserModel.findOne({ email: req.body.email });
    if (user) {
      return next(new ResponseError(userConstants.EMAIL_EXIST));
    }

    const password = await bcrypt.hash(req.body.password, 12);
    const newUser = new UserModel({
      email: req.body.email,
      password: password,
      phoneNumber: req.body.phoneNumber,
      firstName: "",
      lastName: "",
      address: "",
      image: "/uploads/user-default.png",
    });
    await newUser.save();

    return res.status(StatusCodes.OK).json(new ResponseSuccess(true));
  } catch (error) {
    next(new ResponseError(error.message));
  }
};

const login = async (req, res, next) => {
  try {
    const user = await UserModel.findOne({ email: req.body.email });
    if (!user) {
      return next(new ResponseError(userConstants.EMAIL_NOT_EXIST));
    }

    const isValid = await bcrypt.compare(req.body.password, user.password);
    if (!isValid) {
      return next(new ResponseError(userConstants.PASSWORD_INVALID));
    }

    return res.status(StatusCodes.OK).json(
      new ResponseSuccess({
        user: user.toObject(),
        token: jwt.sign({ email: req.body.email }, process.env.SECRET_KEY),
      })
    );
  } catch (error) {
    return next(error.message);
  }
};

const getById = async (req, res, next) => {
  try {
    const user = await UserModel.findById(req.params.id);
    if (!user) {
      return next(new ResponseError(userConstants.USER_NOT_FOUND));
    }

    return res
      .status(StatusCodes.OK)
      .json(new ResponseSuccess(user.toObject()));
  } catch (error) {
    return next(new ResponseError(error.message));
  }
};

const update = async (req, res, next) => {
  try {
    const user = await UserModel.findById(req.params.id);
    if (!user) {
      return next(new ResponseError(userConstants.USER_NOT_FOUND));
    }

    if (user.id !== req.params.id) {
      return next(new ResponseError(userConstants.USER_NOT_ALLOW));
    }

    if (req.file) {
      fs.unlink(user.image, (error) => {});
      user.image = `uploads/images/${req.file.filename}`;
    }

    user.firstName = req.body.firstName;
    user.lastName = req.body.lastName;
    user.address = req.body.address;
    user.phoneNumber = req.body.phoneNumber;
    await user.save();

    return res
      .status(StatusCodes.OK)
      .json(new ResponseSuccess(user.toObject()));
  } catch (error) {
    return next(new ResponseError(error.message));
  }
};

const resetPassword = async (req, res, next) => {
  try {
    const user = await UserModel.findById(req.params.id);
    if (!user) {
      return next(new ResponseError(userConstants.USER_NOT_FOUND));
    }

    const password = await bcrypt.hash(req.body.password, 12);
    user.password = password;
    await user.save();

    return res.status(StatusCodes.OK).json(new ResponseSuccess(true));
  } catch (error) {
    return next(new ResponseError(error.message));
  }
};

export const UserController = {
  register,
  login,
  getById,
  update,
  resetPassword,
};
